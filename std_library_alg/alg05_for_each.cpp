// C++17 standard
// created by cicek on Jun 24, 2021 11:33 PM

#include <algorithm>
#include <array>
#include <iostream>

void doubleNumber(int &i) { i *= 2; }

int main()
{
    std::array arr{ 1, 2, 3, 4 };

    std::for_each(arr.begin(), arr.end(), doubleNumber);
    //    // or
    //    for (auto &i : arr)
    //    {
    //        doubleNumber(i);
    //    }

    for (int i : arr)
    {
        std::cout << i << ' ';
    }

    std::cout << '\n';

    //    std::ranges::for_each(arr, doubleNumber); // Since C++20, we don't have to use begin() and end().
//    std::for_each(arr.begin(), arr.end(), doubleNumber); // Before C++20

    return 0;
}
