// C++17 standard
// created by cicek on Jun 23, 2021 10:46 AM

#include <iostream>

int main()
{
    int myArr[4]{19,29,32,48};

    std::cout << myArr << '\n';         // 0x7ffd23b7a9b0
    std::cout << &(myArr[0]) << '\n';   // 0x7ffd23b7a9b0
    std::cout << &(*myArr) << '\n';     // 0x7ffd23b7a9b0

    std::cout << *(myArr+0) << '\n';    // 19

    std::cout << "sizeof(myArr): " << sizeof(myArr) << '\n'; // will print sizeof(int) * array length
    // 16

    int* ptr{ myArr };
    std::cout << "sizeof(ptr): " << sizeof(ptr) << '\n';   // will print the size of a pointer
    // 8

    return 0;
}
