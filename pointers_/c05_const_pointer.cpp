// C++17 standard
// created by cicek on Jun 24, 2021 12:27 PM

#include <iostream>

int main()
{
    // A pointer to a const value is a (non-const) pointer that points to a constant value. ///

    //    const int value{ 5 };
    //    const int* ptr{ &value }; // this is okay, ptr is a non-const pointer that is pointing to a "const int"

    //    int value1{ 5 };
    //    const int *ptr{ &value1 }; // ptr points to a const int
    //
    //    int value2{ 6 };
    //    ptr = &value2; // okay, ptr now points at some other const int

    /// We can also make a pointer itself constant. A const pointer is a pointer whose value can not be changed after initialization
    //    int value1{ 5 };
    //    int value2{ 6 };
    //
    //    int* const ptr{ &value1 }; // okay, the const pointer is initialized to the address of value1
    //    ptr = &value2; // not okay, once initialized, a const pointer can not be changed.

    int value{ 5 };
    int *const ptr{ &value }; // ptr will always point to value
    *ptr = 6; // allowed, since ptr points to a non-const int

    int myVal = 23;
    int* const myPtr = &myVal;
    int a = 54;

//    myPtr = &a;

    std::cout << *myPtr;

    return 0;
}
