// C++17 standard
// created by cicek on Jun 23, 2021 9:22 AM

#include <iostream>

void foo(int*&) // We cover & later. Don't worry about it for now, we're only using it to trick the compiler into thinking that p has a value.
{
    // p is a reference to a pointer.  We'll cover references (and references to pointers) later in this chapter.
    // We're using this to trick the compiler into thinking p could be modified, so it won't complain about p being uninitialized.
    // This isn't something you'll ever want to do intentionally.
}

int main()
{
    int value{ 99 };
    std::cout << &value << '\n'; // prints address of value
    std::cout << value << '\n'; // prints contents of value

    int *ptr{ &value }; // ptr points to value
    std::cout << ptr << '\n'; // prints address held in ptr, which is &value
    std::cout
        << *ptr
        << '\n'; // Indirection through ptr (get the value that ptr is pointing to)

    /*****/
    int* p; // Create an uninitialized pointer (that points to garbage)
    foo(p); // Trick compiler into thinking we're going to assign this a valid value

    std::cout << *p << '\n'; // Indirection through the garbage pointer
/*****/

    return 0;
}
