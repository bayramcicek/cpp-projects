// C++17 standard
// created by cicek on Jun 23, 2021 7:56 AM

#include <iostream>
#include <typeinfo>

int main()
{
    int a = 5;
    std::cout << a << '\n'; // 5
    std::cout << &a << '\n'; // 0x7ffc56658a14

    // The indirection operator (*)
    // The indirection operator (*) (also called dereference operator) allows
    // us to access the value at a particular address

    std::cout << *(&a) << '\n'; // 5
    /// print the value at the memory address of variable x (parenthesis not required, but make it easier to read)

    // A pointer is a variable that holds a memory address as its value.
    // Note that this asterisk is not an indirection. It is part of the pointer declaration syntax.
    int *iPtr{}; // a pointer to an integer value
    double *dPtr{}; // a pointer to a double value

    int v{ 34 };
    int *ptr{ &v };
    std::cout << ptr << '\n';
    std::cout << *ptr << '\n';
    std::cout << &v << '\n';

    // This is because pointers can only hold addresses, and the integer literal 5 does not have a memory address.
    // If you try this, the compiler will tell you it cannot convert an integer to an integer pointer.
//    int *ptr{ 5 }; // illegal,

//    C++ will also not allow you to directly convert literal memory addresses to a pointer:
//    double* dPtr{ 0x0012FF7C }; // not okay

    std::cout << typeid(&v).name() << '\n'; // Pi ->  (pointer to int)

    return 0;
}
