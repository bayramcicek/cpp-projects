// C++17 standard
// created by cicek on May 04, 2021 9:43 PM

#include <iostream>
#include <cstdlib>

using namespace std;

int doubleNumber(int a) {
    return a*2;
}

int main(int argc, char **argv) {

    /*
     * When the program is executed, the operating system makes a function call to main.
     * Execution then jumps to the top of main. The statements in main are executed sequentially.
     * Finally, main returns an integer value (usually 0), and your program terminates.
     * The return value from main is sometimes called a status code (also sometimes called an
     * exit code, or rarely a return code), as it is used to indicate whether the program ran
     * successfully or not.
     */

    cout << "Hello world\n";


    //Your main function should return 0 if the program ran normally.
    // A non-zero status code is often used to indicate failure

    /*
     * The C++ specification does not define whether function calls evaluate arguments left to right or right to left.
     * Take care not to make function calls where argument order matters.
     */

    int user{};
    cin >> user;
    cout<< doubleNumber(user);


    return EXIT_FAILURE;
}
