// C++17 standard
// created by cicek on May 29, 2021 7:18 AM

#include <iostream>
/*
 * we recommend that this syntax be avoided for normal functions. The return
 * type of a function is of great use in helping to document for the caller
 * what a function is expected to return. When a specific type isn’t specified,
 * the caller may misinterpret what type the function will return, which can lead to inadvertent errors.
 */

// Avoid using type inference for function return types.
auto add(int x, int y) { return x + y; }

//void testFun(auto a) { std::cout << "hey"; } // C++20
// pre-C++20 -> use  function templates

int main()
{
    //  type inference (also sometimes called type deduction).
    auto d{ 5.0 }; // 5.0 is a double literal, so d will be type double
    auto i{ 1 + 2 }; // 1 + 2 evaluates to an int, so i will be type int

    /*
     * In C++14, the auto keyword was extended to be able to deduce a
     * function’s return type from return statements in the function body.
     */

    int c{ 100 };
    c = static_cast<int>(c / 2.5);

    std::cout << c;
    return 0;
}
