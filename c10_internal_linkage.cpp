// C++17 standard
// created by cicek on May 21, 2021 9:34 PM

#include <iostream>

static int
    g_x; // non-constant globals have external linkage by default, but can be given internal linkage via the static keyword

const int g_y{ 1 }; // const globals have internal linkage by default
constexpr int g_z{ 2 }; // constexpr globals have internal linkage by default

// This function is declared as static, and can now be used only within this file
// Attempts to access it from another file via a function forward declaration will fail
static int add(int x, int y) { return x + y; }

int main()
{
    std::cout << "Hello world\n";
    return 0;
}
