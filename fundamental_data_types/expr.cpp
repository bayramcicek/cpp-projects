// C++17 standard
// created by cicek on May 15, 2021 8:59 PM

#include <iostream>
/*
 * Making a function parameter const does two things. First, it tells the
 * person calling the function that the function will not change the value
 * of myValue. Second, it ensures that the function doesn’t change the value of myValue.
 */
void printInteger(const int myValue) { std::cout << myValue; }
/*
 * When arguments are passed by value, we generally don’t care if the function
 * changes the value of the parameter (since it’s just a copy that will be
 * destroyed at the end of the function anyway). For this reason, we usually
 * don’t const parameters passed by value. But later on, we’ll talk about other
 * kinds of function parameters (where changing the value of the parameter will
 * change the value of the argument passed in). For these types of parameters,
 * judicious use of const is important.
 */

int main()
{
    const double gravityq{ 9.8 }; // preferred use of const before type
    int const sidesInSquare{ 4 }; // okay, but not preferred

    constexpr double gravity{
        9.8
    }; // ok, the value of 9.8 can be resolved at compile-time

    std::cout << "Hello world\n";
    /*
     * Any variable that should not be modifiable after initialization and
     * whose initializer is known at compile-time should be declared as constexpr.
Any variable that should not be modifiable after initialization and whose
     initializer is not known at compile-time should be declared as const.
     */

    return 0;
}
