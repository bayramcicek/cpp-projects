// C++17 standard
// created by cicek on May 10, 2021 9:26 PM

#include <iostream>

// In C++, we typically work with “byte-sized” chunks of data.

/*
 * The _t suffix
Many of the types defined in newer versions of C++ (e.g. std::nullptr_t) use
 a _t suffix. This suffix means “type”, and it’s a common nomenclature applied
 to modern types.
If you see something with a _t suffix, it’s probably a type. But many
 types don’t have a _t suffix, so this isn’t consistently applied.
 */

int main()
{
    std::cout << "bool:\t\t" << sizeof(bool) << " bytes\n";
    std::cout << "char:\t\t" << sizeof(char) << " bytes\n";
    std::cout << "wchar_t:\t" << sizeof(wchar_t) << " bytes\n";
    std::cout << "char16_t:\t" << sizeof(char16_t) << " bytes\n"; // C++11 only
    std::cout << "char32_t:\t" << sizeof(char32_t) << " bytes\n"; // C++11 only
    std::cout << "short:\t\t" << sizeof(short) << " bytes\n";
    std::cout << "int:\t\t" << sizeof(int) << " bytes\n";
    std::cout << "long:\t\t" << sizeof(long) << " bytes\n";
    std::cout << "long long:\t" << sizeof(long long) << " bytes\n"; // C++11 only
    std::cout << "float:\t\t" << sizeof(float) << " bytes\n";
    std::cout << "double:\t\t" << sizeof(double) << " bytes\n";
    std::cout << "long double:\t" << sizeof(long double) << " bytes\n";

    /*
bool:		1 bytes
char:		1 bytes
wchar_t:	4 bytes
char16_t:	2 bytes
char32_t:	4 bytes
short:		2 bytes
int:		4 bytes
long:		8 bytes
long long:	8 bytes
float:		4 bytes
double:		8 bytes
long double:	16 bytes
     */

    return 0;
}
