// C++17 standard
// created by cicek on May 15, 2021 6:13 PM

#include <iostream>

int main()
{
    double dv{ 5.0 };
    float fv{ 6.5f };
    std::cout << 9876543.21f << '\n'; // 9.87654e+06

    double zero {0.0};
    double posinf { 5.0 / zero }; // positive infinity
    std::cout << posinf << '\n'; // inf

    double neginf { -5.0 / zero }; // negative infinity
    std::cout << neginf << '\n'; // -inf

    double nan { zero / zero }; // not a number (mathematically invalid)
    std::cout << nan << '\n'; // -nan

    return 0;
}
