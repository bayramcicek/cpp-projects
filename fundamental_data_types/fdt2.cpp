// C++17 standard
// created by cicek on May 11, 2021 12:49 AM

#include <iostream>

void doSomething(unsigned int x)
{
    // Run some code x times
    std::cout << "x is " << x << '\n'; // x is 4294967295
}

int main()
{
    // By default, integers are signed, which means the number’s sign is preserved.
    short s;
    int i;
    long l;
    long long ll;
    // Prefer the shorthand types that do not use the int suffix or signed prefix.
    // For the math inclined, an n-bit signed variable has a range of -(2^(n-1)) to 2^(n-1) - 1.

    unsigned long ul;
    unsigned int ui;
    /*
     * When no negative numbers are required, unsigned integers are well-suited
     * for networking and systems with little memory, because unsigned integers
     * can store more positive numbers without taking up extra memory.
     */

    unsigned short x{ 65535 }; // largest 16-bit unsigned value possible
    std::cout << "x was: " << x << '\n'; // 65535

    x = 65536; // 65536 is out of our range, so we get wrap-around
    std::cout << "x is now: " << x << '\n'; // x is now: 0

    x = 65537; // 65537 is out of our range, so we get wrap-around
    std::cout << "x is now: " << x << '\n'; // x is now: 1

    unsigned int xx{ 3 };
    unsigned int yy{ 5 };

    std::cout << xx - yy << '\n'; // 4294967294
    /*
     * Many developers (and some large development houses, such as Google)
     * believe that developers should generally avoid unsigned integers.
     *
     * This occurs due to -2 wrapping around to a number close to the top of the
     * range of a 4-byte integer. A common unwanted wrap-around happens when an
     * unsigned integer is repeatedly decremented with the -- operator.
     */

    doSomething(-1);

    // . Bjarne Stroustrup, the designer of C++, said, “Using an unsigned
    // instead of an int to gain one more bit to represent positive integers
    // is almost never a good idea”.

    /*
     * Avoid using unsigned numbers, except in specific cases or when unavoidable.

Don’t avoid negative numbers by using unsigned types.
     If you need a larger range than a signed number offers,
     use one of the guaranteed-width integers shown in the next
     lesson (4.6 -- Fixed-width integers and size_t).

If you do use unsigned numbers, avoid mixing signed and unsigned numbers where possible.
     */
    return 0;
}
