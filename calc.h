// C++17 standard
// created by cicek on Dec 04, 2021 10:33 AM

#ifndef CPP_PROJECTS_CALC_H
#define CPP_PROJECTS_CALC_H

int add(int, int);
int sub(int a, int b);
int mul(int a, int b);
int divide(int a, int b);

int calculate(int doCalc, int op1, int op2);

#endif //CPP_PROJECTS_CALC_H
