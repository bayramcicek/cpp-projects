// C++17 standard
// created by cicek on Jun 24, 2021 7:20 PM

#include <iostream>
#include <vector>

int main()
{
    // no need to specify length at the declaration
    std::vector<int> array;
    std::vector<int> array2
        = { 9, 7, 5, 3,
            1 }; // use initializer list to initialize array (Before C++11)
    std::vector<int> array3{
        9, 7, 5, 3, 1
    }; // use uniform initialization to initialize array

    array[6] = 2; // no bounds checking
    array.at(7) = 3; // does bounds checking

    // In this case, the vector will self-resize to match the number of elements provided.
    array = { 0, 1, 2, 3, 4 }; // okay, array length is now 5
    array = { 9, 8, 7 }; // okay, array length is now 3

    return 0;
}
