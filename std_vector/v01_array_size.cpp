// C++17 standard
// created by cicek on Jun 24, 2021 7:32 PM

#include <iostream>
#include <vector>

void printLength(const std::vector<int> &array)
{
    std::cout << "The length is: " << array.size() << '\n';
}

int main()
{
    std::vector array{ 9, 7, 5, 3, 1, 7, 6, 5 };
    printLength(array);
    return 0;
}
