// C++17 standard
// created by cicek on Jun 27, 2021 11:06 AM

#include <iostream>
#include <vector>

void printStack(const std::vector<int> &stack)
{
    for (auto element : stack)
        std::cout << element << ' ';
    std::cout << "(cap " << stack.capacity() << " length " << stack.size()
              << ")\n";
}

int main()
{
    std::vector<int> stack{};

    // Because resizing the vector is expensive, we can tell the vector to
    // allocate a certain amount of capacity up front using the reserve() function:
    stack.reserve(5); // Set the capacity to (at least) 5

    printStack(stack);

    stack.push_back(5);
    printStack(stack);

    stack.push_back(3);
    printStack(stack);

    stack.push_back(2);
    printStack(stack);

    std::cout << "top: " << stack.back() << '\n';

    stack.pop_back();
    printStack(stack);

    stack.pop_back();
    printStack(stack);

    stack.pop_back();
    printStack(stack);

    return 0;
}
