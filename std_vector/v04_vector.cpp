// C++17 standard
// created by cicek on Jun 27, 2021 10:41 AM

#include <iostream>
#include <vector>

//  length is how many elements are being used in the array,
//  capacity is how many elements were allocated in memory.

int main()
{
    std::vector<int> array45{ 0, 1, 2 };
    array45.resize(4); // set length to 5

    //    std::cout << "The length is: " << array.size() << '\n';
    //
    //    for (auto element : array)
    //        std::cout << element << ' ';

    std::cout << "\nThe length is: " << array45.size() << '\n';
    std::cout << "The capacity is: " << array45.capacity() << '\n';

    return 0;
}