// C++17 standard
// created by cicek on Jun 24, 2021 7:17 PM

#include <iostream>

struct House
{
    int number{};
    int stories{};
    int roomsPerStory{};
};

struct Array
{
    House value[3]{};
};

int main()
{
    // With braces, this works.
    Array houses{ { { 13, 4, 30 }, { 14, 3, 10 }, { 15, 3, 40 } } };

    for (const auto &house : houses.value)
    {
        std::cout << "House number " << house.number << " has "
                  << (house.stories * house.roomsPerStory) << " rooms\n";
    }

    /*
House number 13 has 120 rooms
House number 14 has 30 rooms
House number 15 has 120 rooms
     *
     */

    return 0;
}
