// C++17 standard
// created by cicek on May 05, 2021 6:58 PM

#include <iostream>

// “std” (short for standard).
using namespace std;
// Another way to access identifiers inside a namespace is to use a "using directive" statement.

//int cout() { return 4; }

int main()
{
    // when we say cout, we mean the cout defined in the std namespace
    // So when we say std::cout, we’re saying “the cout that lives in namespace std“.
    cout << "Hello world\n";
    // Compile error!  Which cout do we want here?  The one in the std namespace or
    // the one we defined above? int cout() { return 4; }

    /*
     * The :: symbol is an operator called the "scope resolution operator".
     * The identifier to the left of the :: symbol identifies the namespace that
     * the name to the right of the :: symbol is contained within. If no identifier to
     * the left of the :: symbol is provided, the global namespace is assumed.
     */

    // This is the safest way to use cout, because there’s no ambiguity about which
    // cout we’re referencing (the one in the std namespace).

    /*
     * Best practice
Use explicit namespace prefixes to access identifiers defined in a namespace.
     */

    /*
     * Warning

Avoid using directives (such as using namespace std;) at the top of your program.
     They violate the reason why namespaces were added in the first place.
     */

    return 0;
}
