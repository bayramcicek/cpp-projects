// C++17 standard
// created by cicek on Jun 24, 2021 8:21 AM

#include <iostream>

int main()
{
    int *ptr{ new int };
    *ptr = 34;

    std::cout << ptr << ": " << *ptr << '\n'; // 0x563ed8e9ceb0: 34

    int *p_var{ new int(6) };
    int *p_val{ new int{ 78 } };

    std::cout << *p_var << '\n';
    std::cout << *p_val << '\n';

    /*
     * When we are done with a dynamically allocated variable,
     * we need to explicitly tell C++ to free the memory for reuse.
     * For single variables, this is done via the scalar (non-array) form of the delete operator:*/

    delete ptr;
    p_var = nullptr;
    p_val = nullptr;

    return 0;
}
