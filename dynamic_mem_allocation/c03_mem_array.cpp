// C++17 standard
// created by cicek on Jun 24, 2021 10:25 AM

#include <iostream>

int main()
{
    std::cout << "Enter a positive integer: ";
    std::size_t length{};
    std::cin >> length;

    int *array{ new int[length]{} };
    std::cout << "I just allocated an array of integers of length " << length
              << '\n';

    array[0] = 5; // set element 0 to value 5

    delete[] array; // use array delete to deallocate array

    // we don't need to set array to nullptr/0 here because it's going to go out of scope immediately after this anyway

    return 0;
}
