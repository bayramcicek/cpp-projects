// C++17 standard
// created by cicek on Jun 24, 2021 10:42 AM

#include <iostream>

int main()
{
//    int fixedArray[5] = { 9, 7, 5, 3, 1 }; // initialize a fixed array before C++11
//    int *array{ new int[5]{ 9, 7, 5, 3, 1 } }; // initialize a dynamic array since C++11
//// To prevent writing the type twice, we can use auto. This is often done for types with long names.
//    auto *array{ new int[5]{ 9, 7, 5, 3, 1 } };

    std::cout << "Hello world\n";
    return 0;
}
