// C++17 standard
// created by cicek on May 20, 2021 11:27 PM

#include "circle.h"
#include "growth.h"
#include <iostream>

int main()
{
    std::cout << basicMath::pi << "\n";
    std::cout << basicMath::e << "\n";
    return 0;
}
