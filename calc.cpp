// C++17 standard
// created by cicek on Dec 04, 2021 10:31 AM

#include "calc.h"
#include <iostream>

int add(int a, int b) { return a + b; }
int sub(int a, int b) { return a - b; }
int mul(int a, int b) { return a * b; }
int divide(int a, int b) { return a / b; }

int calculate(int doCalc, int op1, int op2)
{
    switch (doCalc)
    {
        case 1:
            return add(op1, op2);
        case 2:
            return sub(op1, op2);
        case 3:
            return mul(op1, op2);
        case 4:
            return divide(op1, op2);

        default:
            std::cout << "please enter numbers between 1-4";
            return -1;
    }
}
