// C++17 standard
// created by cicek on Jun 27, 2021 2:50 PM

// Program: MyArgs
#include <iostream>

int main(int argc, char *argv[])
{
    std::cout << "There are " << argc << " arguments:\n";

    // Loop through each argument and print its number and value
    for (int count{ 0 }; count < argc; ++count)
    {
        std::cout << count << ' ' << argv[count] << '\n';
    }

    return 0;
}