// C++17 standard
// created by cicek on Jun 20, 2021 11:42 AM

#include <iostream>

/*
 * The process of converting a value from one data type to another data type is called a type conversion.
Type conversion can be invoked in one of two ways: either implicitly (as needed by the compiler),
 or explicitly (when requested by the programmer).

 Implicit type conversion (also called automatic type conversion or coercion)
 */

int add(int x, int y) // integer version
{
    return x + y;
}

double add(double x, double y) // floating point version
{
    return x + y;
}

int main()
{
    std::cout << "Hello world\n";

    double division{ 4.0 / 3 }; // int value 3 implicitly converted to type double

    // int x { 3.5 }; // brace-initialization disallows conversions that result in data loss

    // Numeric promotion reduces redundancy

    /*
     *  And don’t forget another version for unsigned char, signed char,
     *  unsigned short, wchar_t, char8_t, char16_t, and char32_t! You can see how this quickly becomes unmanageable.
     */

    /*
     * Implicit type conversion is automatically performed whenever one data type is expected, but a different data type is supplied.
Explicit type conversion happens when the programmer uses a type cast to explicitly convert a value from one type to another type.
     */

    typedef double distance_t;
    distance_t testDoubleValue = 4.7;

    auto varA{ add(5.6, 2.8)};
    std::cout << varA << '\n';



    return 0;
}



