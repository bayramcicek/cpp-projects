// C++17 standard
// created by cicek on Jun 21, 2021 9:25 AM

#include <iostream>

namespace MyAnimals
{
enum ArrayAnimal
{
    chicken,
    dog,
    cat,
    elephant,
    duck,
    snake,
    animal_top_num
};
}

void printSize(int array[])
{
    std::cout << sizeof(array) / sizeof(array[0]) << '\n'; // 2
}

int main()
{
    std::cout << "Hello world\n";

    int myArray[5] = { 1, 2, 3, 4, 5 };

    std::cout << myArray[1] << '\n';
    std::cout << std::size(myArray) << '\n'; // 5
    std::cout << sizeof(myArray) << '\n'; // 20 byte
    std::cout << "number of elements: "
              << (sizeof(myArray) / sizeof(myArray[0])) << '\n';
    printSize(myArray);

    int prime[5]{}; // hold the first 5 prime numbers
    prime[5] = 13;

    std::cout << prime[5] << '\n';

    double temperature[365]{};

    int legs[MyAnimals::animal_top_num]{ 2, 4, 4, 4, 2, 0 };
    std::cout << legs[MyAnimals::elephant];

    return 0;
}
