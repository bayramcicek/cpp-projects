// C++17 standard
// created by cicek on May 21, 2021 10:08 PM

#include <iostream>

int g_x{ 2 }; // non-constant globals are external by default

extern const int g_y{
    3
}; // const globals can be defined as extern, making them external
extern constexpr int g_z{
    3
}; // constexpr globals can be defined as extern, making them external (but this is useless, see the note in the next section)
// We use const instead of constexpr in this method because constexpr variables can’t be forward declared, even if they have external linkage.

/*

// non-constant
int g_x; // variable definition (can have initializer if desired)
extern int g_x; // forward declaration (no initializer)

// constant
extern const int g_y { 1 }; // variable definition (const requires initializers)
extern const int g_y; // forward declaration (no initializer)

*/

int main()
{
    std::cout << "Hello world\n";
    return 0;
}
