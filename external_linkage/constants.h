// C++17 standard
// created by cicek on May 24, 2021 1:45 AM

#ifndef CPP_PROJECTS_CONSTANTS_H
#define CPP_PROJECTS_CONSTANTS_H

// define your own namespace to hold constants
namespace constants
{
// constants have internal linkage by default
constexpr double pi { 3.14159 };
constexpr double avogadro { 6.0221413e23 };
constexpr double my_gravity { 9.2 }; // m/s^2 -- gravity is light on this planet
// ... other related constants
}

#endif //CPP_PROJECTS_CONSTANTS_H
