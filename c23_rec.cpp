// C++17 standard
// created by cicek on Jun 27, 2021 2:19 PM

// h/t to reader Donlod for this solution
#include <iostream>

void printBinary(unsigned int n)
{
    // we only recurse if n > 1, so this is our termination case for n == 0
    if (n > 1)
    {
        printBinary(n / 2);
    }

    std::cout << n % 2;
}

int main()
{
    int x{};
    std::cout << "Enter an integer: ";
    std::cin >> x;

    std::cout << static_cast<unsigned int>(x);

    //    printBinary(static_cast<unsigned int>(x));
    // https://www.learncpp.com/cpp-tutorial/recursion/
}