// C++17 standard
// created by cicek on Jun 29, 2021 11:36 AM

#include <iostream>

struct DateStruct
{
    int year{};
    int month{};
    int day{};
};

void print(const DateStruct &date)
{
    std::cout << date.year << '/' << date.month << '/' << date.day;
}

int main()
{
    DateStruct today{ 2020, 10, 14 }; // use uniform initialization

    today.day
        = 16; // use member selection operator to select a member of the struct
    print(today);

    /*
struct DateStruct
{
    int year{};
    int month{};
    int day{};
};

class DateClass
{
public:
    int m_year{};
    int m_month{};
    int m_day{};
};
     Just like a struct declaration, a class declaration does not allocate any memory.
     It only defines what the class looks like.

     DateClass today { 2020, 10, 14 }; // declare a variable of class DateClass
     */

    return 0;
}