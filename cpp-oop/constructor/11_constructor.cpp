// C++17 standard
// created by cicek on Jul 03, 2021 10:54 AM

#include <iostream>
// Use member initializer lists to initialize your class member variables instead of assignment.
class Something
{
private:
    const int m_value;

public:
    Something()
        : m_value{ 5 } // directly initialize our const member variable
    {
    }
};

int main()
{
    std::cout << "Hello, World!" << '\n';
    return 0;
}
