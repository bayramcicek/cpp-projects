// C++17 standard
// created by cicek on Oct 11, 2021 7:34 PM

#include <functional> // std::reference_wrapper
#include <iostream>
#include <string>
#include <vector>

// reference wrapper
int main()
{
    std::string tom{ "Tom" };
    std::string berta{ "Berta" };

    std::vector<std::reference_wrapper<std::string>> names{
        tom, berta
    }; // these strings are stored by reference, not value

    std::string jim{ "Jim" };
    names.emplace_back(jim);

    for (auto name : names)
    {
        name.get() += " Cicek";
    }

    for (auto a : names)
    {
        std::cout << a.get() << '\n';
    }
    return 0;
}
