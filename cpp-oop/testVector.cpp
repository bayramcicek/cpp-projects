// C++17 standard
// created by cicek on Jul 20, 2021 10:03 AM

#include <iostream>
#include <vector>
#include<bits/stdc++.h>
int main()
{
//    //    std::vector<std::vector<int>> vec(m, std::vector<int> (n, 0));
    std::vector<std::vector<std::pair<int, std::string>>> vec;

    std::vector<std::pair<int, std::string>> tempvect;

    tempvect.emplace_back(std::pair(3, "sas"));
    vec.push_back(tempvect);

    tempvect.clear();

    tempvect.emplace_back(std::pair(11111, "qweqweqwe"));
    vec.push_back(tempvect);

    tempvect.clear();

    tempvect.emplace_back(std::pair(544, "b"));
    vec.push_back(tempvect);

    for (auto& a: vec)
    {
        std::cout << a[0].first << ' ' << a[0].second << '\n';
    }

    for (auto& a: vec)
    {
        if (a[0].first == 11111)
        {
            std::vector<std::pair<int, std::string>> tVect;
            tVect.emplace_back(std::pair(a[0].first, a[0].second));
            vec.erase(std::remove(vec.begin(), vec.end(), tVect), vec.end());
            break;
        }
    }
    std::cout << '\n';
    for (auto a: vec)
    {
        std::cout << a[0].first << ' ' << a[0].second << '\n';
    }



//
//    std::cout << "\n\n";
////    vec[0][0].first = 234;
////    vec[0][0].second = "hello";
////    std::cout << vec[0][0].first << '\n';
////    std::cout << vec[0][0].second << '\n';
//
//
//    //    vec.reserve(110);
//    //    for (int i = 0; i < 110; ++i)
//    //    {
//    //        vec.push_back(tempvect);
//    //    }
//
////    vec.erase(vec.begin()+1);
//    vec.erase(std::remove(vec.begin(), vec.end(), 3), vec.end());
//
//    for (auto a: vec)
//    {
//        std::cout << a[0].first << ' ' << a[0].second << '\n';
//    }

//    std::vector<int> v;
//    //Insert values 1 to 10
//    v.push_back(1);
//    v.push_back(3);
//    v.push_back(44);
//    v.push_back(666);
//    v.push_back(6767);
//    v.push_back(8989);
//    v.emplace_back(9);
//
//    int item = 666;
//
//    v.erase(std::remove(v.begin(), v.end(), 3), v.end());
//
//    for(int i : v){
//        std::cout << i << " ";
//    }

    return 0;
}
