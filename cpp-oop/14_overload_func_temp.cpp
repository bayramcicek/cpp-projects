// C++17 standard
// created by cicek on Jul 14, 2021 12:03 PM

#include <iostream>

template <class T> T average(T *myArray, int numValues)
{
    T sum{ 0 };
    for (int count = 0; count < numValues; ++count)
        sum += myArray[count];

    sum /= numValues;
    return sum;
}

class Cents
{
private:
    int m_cents;

public:
    explicit Cents(int cents)
        : m_cents(cents)
    {
    }

    friend bool operator>(Cents &c1, Cents &c2)
    {
        return (c1.m_cents > c2.m_cents);
    }

    friend std::ostream &operator<<(std::ostream &out, const Cents &cents)
    {
        out << cents.m_cents << " cents ";
        return out;
    }

    Cents &operator+=(Cents cents)
    {
        m_cents += cents.m_cents;
        return *this;
    }

    Cents &operator/=(int x)
    {
        m_cents /= x;
        return *this;
    }
};

int main()
{
    Cents centsArray[] = { Cents(5), Cents(10), Cents(15), Cents(14) };
    std::cout << average(centsArray, 4) << '\n';

    Cents aCent(100);
    aCent.operator+=(Cents(15));
    aCent += Cents(15);

    std::cout << aCent;

    return 0;
}
