// C++17 standard
// created by cicek on Dec 18, 2021 10:48 PM

#include <iostream>
// Whenever you are dealing with inheritance, you should make any explicit destructors virtual.

class Base
{
public:  // virtual ~Base() = default; // generate a virtual default destructor
    virtual ~Base() // note: virtual
    {
        std::cout << "Calling ~Base()\n";
    }
};

class Derived : public Base
{
private:
    int *m_array;

public:
    Derived(int length)
        : m_array{ new int[length] }
    {
    }

    virtual ~Derived() // note: virtual
    {
        std::cout << "Calling ~Derived()\n";
        delete[] m_array;
    }
};

int main()
{
    Derived *derived{ new Derived(5) };
    Base *base{ derived };

    delete base;

    return 0;
}
