// C++17 standard
// created by cicek on Feb 23, 2022 7:43 PM

#include <iostream>

class Base
{
public:
    Base() {}
};

class Derived : public Base
{
public:
    Derived() {}
};

int main()
{
    try
    {
        throw Derived();
    }
    // Handlers for derived exception classes should be listed before those for base classes.
    catch (const Derived &derived) // most-derived one caught first
    {
        std::cerr << "caught Derived";
    }
    catch (const Base &base)
    {
        std::cerr << "caught Base";
    }

    return 0;
}
