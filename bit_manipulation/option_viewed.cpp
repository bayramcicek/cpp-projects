// C++17 standard
// created by cicek on May 19, 2021 9:58 PM

#include <bitset>
#include <cstdint>
#include <iostream>

int main()
{
    constexpr std::uint_fast8_t option_viewed{ 0x01 };
    constexpr std::uint_fast8_t option_edited{ 0x02 };
    constexpr std::uint_fast8_t option_favorited{ 0x04 };
    constexpr std::uint_fast8_t option_shared{ 0x08 };
    constexpr std::uint_fast8_t option_deleted{ 0x10 };

    std::uint_fast8_t myArticleFlags{ option_favorited };

    // ...
    myArticleFlags |= option_viewed;
    myArticleFlags |= option_deleted;

    if (myArticleFlags & option_deleted)
        std::cout << "deleted" << '\n';

    myArticleFlags &= ~option_favorited;

    std::cout << std::bitset<8>{ myArticleFlags } << '\n';
    return 0;
}

/*
 * a) Write a line of code to set the article as viewed.
Expected output:

00000101
 */