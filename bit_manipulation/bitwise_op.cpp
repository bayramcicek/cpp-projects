// C++17 standard
// created by cicek on May 18, 2021 7:37 PM

#include <bitset>
#include <iostream>

int main()
{
    std::bitset<4> x{ 0b0110 };

    std::cout << x << '\n'; // 0110
    std::cout << (x << 1) << '\n'; // 1100
    std::cout << (x >> 1) << '\n'; // 0011

    // Note that if you're using operator << for both output and left shift, parenthesization is required
    std::cout << x << 1 << '\n'; // print value of x (0110), then 1
    std::cout << (x << 1) << '\n'; // print x left shifted by 1 (1100)

    std::cout << std::bitset<4>{ ~0b0100u } << ' ' << std::bitset<8>{ ~0b0100u }
              << '\n'; // 1011 11111011

    // bitwise or
    std::cout << (std::bitset<4>{ 0b0101 } | std::bitset<4>{ 0b0110 }) << '\n';

    // and
    std::cout << (std::bitset<4>{ 0b0101 } & std::bitset<4>{ 0b0110 }) << '\n';

    // bitwise XOR (^), also known as exclusive or.
    std::cout << (std::bitset<4>{ 0b0101 } ^ std::bitset<4>{ 0b0110 }) << '\n';

    // Bitwise assignment operators
    // For example, instead of writing x = x >> 1;, you can write x >>= 1;.
    std::bitset<4> my_bitset{ 0b0011 };
    my_bitset >>= 1;

    std::cout << my_bitset << '\n'; // 0001

    my_bitset <<= 3;
    std::cout << my_bitset << '\n'; // 1000
    return 0;
}
