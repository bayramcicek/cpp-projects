#include <iostream>

/*
 * Line 1 is a special type of line called a preprocessor directive.
 * This preprocessor directive indicates that we would like to use the
 * contents of the iostream library, which is the part of the C++ standard
 * library that allows us to read and write text from/to the console.
 * We need this line in order to use std::cout on line 5. Excluding this
 * line would result in a compile error on line 5, as the compiler wouldn’t
 * otherwise know what std::cout is.
 */

int main() {
    /*
     *  std::cout (which stands for “character output”)
     *  and the << operator allow us to send letters or numbers to the console to be output.
     */
    std::cout << "Hello, World!" << 2 << std::endl;  //  cout stands for “character output”.
    //  insertion operator (<<)

    int val{34};

    std::cout << val << std::endl;
    // define integer variable x, initialized with value 34

    /*
     * Line 6 is a return statement. When an executable program finishes running,
     * the program sends a value back to the operating system in order to indicate whether
     * it ran successfully or not. This particular return statement returns the value of 0
     * to the operating system, which means “everything went okay!”. This is the last statement
     * in the program that executes.
     */

    /*
     * std::cin is another predefined variable that is defined in the iostream library.
     * Whereas std::cout prints data to the console using the insertion operator (<<), std::cin
     * (which stands for “character input”) reads input from keyboard using the
     * extraction operator (>>). The input must be stored in a variable to be used.
     */
    std::cout << "enter a number: ";
    int a{}; // define variable x to hold user input (and zero-initialize it)
    std::cin >> a;
    std::cout << "you entered: " << a << '\n';

    return 0;
}
