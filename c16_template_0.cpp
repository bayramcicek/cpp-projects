// C++17 standard
// created by cicek on Jun 20, 2021 11:37 PM

#include <iostream>

// <class T>
template <typename T> // this is the template parameter declaration
T max(T x, T y) // this is the function template definition for max<T>
{
    return x + y;
}

template <typename T, typename U>
auto max1(T x, U y)
{
    return (x > y) ? x : y;
}


int main()
{
    std::cout << "Hello world\n";

    auto a = max(5.6, 5.3);
    auto b = max(5, 4);
    //  cout stands for “character output”.
    std::cout << a << '\n';
    std::cout << b << '\n';

    // function template instantiation (or instantiation for short).
    // // instantiates and calls function max<int>(int, int)
    std::cout << max<float>(3.2f, 2.8f);

    std::cout << max<int>(1, 2) << '\n'; // instantiates and calls function max<int>(int, int)
    std::cout << max<int>(4, 3) << '\n'; // calls already instantiated function max<int>(int, int)
    std::cout << max<double>(1, 2) << '\n'; // instantiates and calls function max<double>(double, double)

    // template argument deduction
    std::cout << max<>(1, 2) << '\n';
    std::cout << max(1, 2) << '\n';

    std::cout << max(static_cast<double>(2), 3.5) << '\n'; // convert our int to a double, so we can call max(double, double)
    std::cout << max<double>(2, 3.5) << '\n'; // we've provided actual type double, so the compiler won't use template argument deduction

    // Making the return type a U instead doesn’t solve the problem, as we can always flip the order of the operands in the function call to flip the types of T and U.
    std::cout << max1(2, 3.5) << '\n'; // max(2, 4.5) -> error

    /*
     * C++20 introduces a new use of the auto keyword: abbreviated function template.
     *
     * For example:

auto max(auto x, auto y)
{
    return (x > y) ? x : y;
}
is shorthand in C++20 for the following:

template <typename T, typename U>
auto max(T x, U y)
{
    return (x > y) ? x : y;
}
which is the same as the max function template we wrote above.
     *
     *
     */






    return 0;
}
