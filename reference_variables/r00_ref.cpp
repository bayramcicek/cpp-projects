// C++17 standard
// created by cicek on Jun 24, 2021 12:57 PM

#include <iostream>

int main()
{
    /*
     * References are the third basic type of variable that C++ supports.
     * A reference is a type of C++ variable that acts as an alias to another object or value.
     */

    /*
     C++ supports three kinds of references:

References to non-const values (typically just called “references”, or “non-const references”), which we’ll discuss in this lesson.
References to const values (often called “const references”), which we’ll discuss in the next lesson.
C++11 added r-value references, which we cover in detail in the chapter on move semantics.
     */

    int value{ 5 }; // normal integer
    int &ref{ value }; // reference to variable value

    value = 6; // value is now 6
    ref = 7; // value is now 7

    std::cout << value << '\n'; // prints 7
    ++ref;
    std::cout << value << '\n'; // prints 8

    std::cout << &value << '\n'; // prints 0012FF7C
    std::cout << &ref << '\n'; // prints 0012FF7C

    //    int value{ 5 };
    //    int& ref{ value }; // valid reference, initialized to variable value
    //
    //    int& invalidRef; // invalid, needs to reference something

    //    int x{ 5 };
    //    int& ref1{ x }; // okay, x is an non-const l-value
    //
    //    const int y{ 7 };
    //    int& ref2{ y }; // not okay, y is a const l-value
    //
    //    int& ref3{ 6 }; // not okay, 6 is an r-value

    //    int value1{ 5 };
    //    int value2{ 6 };
    //
    //    int& ref{ value1 }; // okay, ref is now an alias for value1
    //    ref = value2; // assigns 6 (the value of value2) to value1 -- does NOT change the reference!

    return 0;
}
